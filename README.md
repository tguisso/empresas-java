# Sistema Empresas Java - Best Movie

# Requisitos Atendidos

- A API construída em Java 11 utilizando Spring Framework 2.4.1
- Operações no banco de dados utilizando ***Spring Data JPA*** & ***Hibernate***
- Uso do *MySQL*
- Padrões REST na construção das rotas e retornos
- Documentação viva utilizando a *OpenAPI Specification* (**Swagger**)
- Testes unitários
- Estrutura do projeto
- Utilização de código limpo e princípios **SOLID**
- Boas práticas da Linguagem/Framework

# Requisitos Não Atendidos

- Implementar autenticação seguindo o padrão ***JWT***, lembrando que o token a ser recebido deve estar no formado ***Bearer***
- As entidades deversão ser criadas como tabelas utilizando a ferramenta de migração **Flyway**. Portanto, os scripts de **migrations** para geração das tabelas devem ser enviados no teste
- Teste de integração da API em linguagem de sua preferência (damos importância para pirâmide de testes)
- Cobertura de testes utilizando Sonarqube
- Utilização de *Docker* (enviar todos os arquivos e instruções necessárias para execução do projeto)
- Segurança da API, como autenticação, senhas salvas no banco, *SQL Injection* e outros
- Opção de trazer registros paginados

# Extra
- Clean architecture

# INSTRUÇÕES
- Maven:  para build do projeto. Para buildar: mvn clean install
- Foi utilizado Lombok, portanto é necessário adicionar o plugin na IDE

- Para acesso da documentação da API, utilize o endereço:

	http://localhost:8080/swagger-ui.html#/

# Exemplos de Payloads

USER - CREATE

```js
curl --location --request POST 'http://localhost:8080/users' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "UYUY",
    "username": "GH",
    "password": "ggdfd",
    "userType": "USER"
}'
```

USER - UPDATE

```js
curl --location --request PUT 'http://localhost:8080/users/3' \
--header 'username: anyUsername' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Fulano",
    "username": "anyUsername",
    "password": "ggdfd",
    "userType": "ADMIN"
}'
```

USER - GET ACTIVES

```js
curl --location --request GET 'http://localhost:8080/users/actives' \
--header 'username: anyUsername'
```

USER - GET DELETE

```js
curl --location --request DELETE 'http://localhost:8080/users/1' \
--header 'username: anyUsername'
```

MOVIE CREATE

```js
curl --location --request POST 'http://localhost:8080/movies' \
--header 'username: anyUsername' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Terminator 2",
    "actors": [{
        "id": 1,
        "name": "Shuarneggers"
    },
    {
        "id": null,
        "name": "Sarha Cronors"
    }
    
    ],
    "director": {
        "id": 10,
        "name": "Sprilgehs",
        "movies": []
    },
    "gender": "MUSICAL",
    "votes": []
}'
```

MOVIE VOTE

```js
curl --location --request POST 'http://localhost:8080/movies/7/vote' \
--header 'username: anyUsername' \
--header 'Content-Type: application/json' \
--data-raw '{
   "vote": 0,
   "userId": 3
}'

```

MOVIE GET BY NAME

```js
curl --location --request GET 'http://localhost:8080/users/actives' \
--header 'username: anyUsername'

```

MOVIE GET BY DIRECTOR NAME

```js
curl --location --request GET 'http://localhost:8080/movies/directors/name/Pete Docter' \
--header 'username: anyUsername'

```

MOVIE GET BY GENDER

```js
curl --location --request GET 'http://localhost:8080/movies/gender/ANIMATION' \
--header 'username: anyUsername'

```
