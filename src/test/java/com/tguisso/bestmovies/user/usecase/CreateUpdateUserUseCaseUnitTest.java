package com.tguisso.bestmovies.user.usecase;

import static com.tguisso.bestmovies.test.databuilder.DataBuilderBase.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.springframework.test.util.ReflectionTestUtils.setField;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.bestmovies.test.databuilder.domain.UserDataBuilder;
import com.tguisso.bestmovies.user.domain.User;
import com.tguisso.bestmovies.user.gateway.database.UserDBGateway;
import com.tguisso.bestmovies.user.usecase.exception.UnauthorizedUserBusinessException;
import com.tguisso.bestmovies.user.usecase.exception.UserAlreadyExistsBusinessException;
import com.tguisso.bestmovies.user.usecase.exception.UserNotFoundBusinessException;

@ExtendWith(MockitoExtension.class)
public class CreateUpdateUserUseCaseUnitTest {
	
	@InjectMocks
	private CreateUpdateUserUseCase createUpdateUserUseCase;
	
	@Mock
	private UserDBGateway userDBGateway;
	
	@Test
	public void createWithSuccess() {
		final User user = UserDataBuilder.getAnyAdmin();
		setField(user, "id", null);
		
		final Long idToReturn = getRandomLong();
		doReturn(idToReturn).when(userDBGateway).save(user);
		
		final Long generatedId = createUpdateUserUseCase.create(user);
		
		verify(userDBGateway).save(user);
		verify(userDBGateway).findUserByUsername(user.getUsername());
		
		assertEquals(idToReturn, generatedId);
	}
	
	@Test
	public void createUserAlreadyExistsBusinessException() {
		final User user = UserDataBuilder.getAnyAdmin();
		setField(user, "id", null);
		
		final Optional<User> userFound = Optional.of(user);
		doReturn(userFound).when(userDBGateway).findUserByUsername(user.getUsername());
		
		assertThrows(UserAlreadyExistsBusinessException.class, () -> {
			createUpdateUserUseCase.create(user);
		});
		
		verify(userDBGateway, never()).save(user);
		verify(userDBGateway).findUserByUsername(user.getUsername());
		
	}
	
	@Test
	public void updateWithSuccess() {
		final User user = UserDataBuilder.getAnyAdmin();
		
		final Optional<User> userFound = Optional.of(user);
		doReturn(userFound).when(userDBGateway).findUserByUsername(user.getUsername());
		
		createUpdateUserUseCase.update(user, user.getUsername(), user.getId());
		
		verify(userDBGateway).findUserByUsername(user.getUsername());
		verify(userDBGateway).save(user);
		
	}
	
	@Test
	public void updateUserNotFoundException() {
		final User user = UserDataBuilder.getAnyAdmin();
		
		final Optional<User> userFound = Optional.empty();
		doReturn(userFound).when(userDBGateway).findUserByUsername(user.getUsername());
		
		assertThrows(UserNotFoundBusinessException.class, () -> {
			createUpdateUserUseCase.update(user, user.getUsername(), user.getId());
		});
		
		verify(userDBGateway).findUserByUsername(user.getUsername());
		verify(userDBGateway, never()).save(user);
	}
	
	@Test
	public void updateUnauthorizedUserException() {
		final User user = UserDataBuilder.getAnyAdmin();
		final User userToReturn = UserDataBuilder.getAnyAdmin();
		setField(userToReturn, "username", user.getUsername());
		
		final Optional<User> userFound = Optional.of(userToReturn);
		doReturn(userFound).when(userDBGateway).findUserByUsername(user.getUsername());
		
		assertThrows(UnauthorizedUserBusinessException.class, () -> {
			createUpdateUserUseCase.update(user, user.getUsername(), user.getId());
		});
		
		verify(userDBGateway).findUserByUsername(user.getUsername());
		verify(userDBGateway, never()).save(user);
	}
	
}
