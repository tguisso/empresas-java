package com.tguisso.bestmovies.user.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.bestmovies.test.databuilder.domain.UserDataBuilder;
import com.tguisso.bestmovies.user.domain.User;
import com.tguisso.bestmovies.user.gateway.database.UserDBGateway;
import com.tguisso.bestmovies.user.usecase.exception.UnauthorizedUserBusinessException;
import com.tguisso.bestmovies.user.usecase.exception.UserNotFoundBusinessException;

@ExtendWith(MockitoExtension.class)
public class GetUserUseCaseUnitTest {
	
	@InjectMocks
	private GetUserUseCase getUserUseCase;
	
	@Mock
	private UserDBGateway userDBGateway;
	
	@Test
	public void get() {
		final User userAdmin = UserDataBuilder.getAnyAdmin();
		final User userA = UserDataBuilder.getAnyUser();
		final User userB = UserDataBuilder.getAnyUser();
		final User userC = UserDataBuilder.getAnyUser();
		final User userD = UserDataBuilder.getAnyUser();
		final List<User> usersToReturn = Arrays.asList(userA, userB, userC, userD);
		doReturn(usersToReturn).when(userDBGateway).findAllActiveUser();
		
		final Optional<User> userOp = Optional.of(userAdmin);
		doReturn(userOp).when(userDBGateway).findUserByUsername(userAdmin.getUsername());
		
		final List<User> usersReturned = getUserUseCase.getAllActive(userAdmin.getUsername());
		
		verify(userDBGateway).findUserByUsername(userAdmin.getUsername());
		verify(userDBGateway).findAllActiveUser();
		
		assertEquals(4, usersReturned.size());
		assertEquals(usersToReturn.get(0), usersReturned.get(0));
		assertEquals(usersToReturn.get(1), usersReturned.get(1));
		assertEquals(usersToReturn.get(2), usersReturned.get(2));
		assertEquals(usersToReturn.get(3), usersReturned.get(3));
	}
	
	@Test
	public void getNonExistentUserException() {
		final User userAdmin = UserDataBuilder.getAnyAdmin();
		
		final Optional<User> userOp = Optional.empty();
		doReturn(userOp).when(userDBGateway).findUserByUsername(userAdmin.getUsername());
		
		assertThrows(UserNotFoundBusinessException.class, () -> {
			getUserUseCase.getAllActive(userAdmin.getUsername());
		});
		
		verify(userDBGateway).findUserByUsername(userAdmin.getUsername());
		verify(userDBGateway, never()).findAllActiveUser();
	}
	
	@Test
	public void getUnauthorizedUserException() {
		final User commonUser = UserDataBuilder.getAnyUser();
		
		final Optional<User> userOp = Optional.of(commonUser);
		doReturn(userOp).when(userDBGateway).findUserByUsername(commonUser.getUsername());
		
		assertThrows(UnauthorizedUserBusinessException.class, () -> {
			getUserUseCase.getAllActive(commonUser.getUsername());
		});
		
		verify(userDBGateway).findUserByUsername(commonUser.getUsername());
		verify(userDBGateway, never()).findAllActiveUser();
	}

}
