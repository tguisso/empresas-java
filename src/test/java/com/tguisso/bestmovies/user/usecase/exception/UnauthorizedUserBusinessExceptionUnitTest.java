package com.tguisso.bestmovies.user.usecase.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

@ExtendWith(MockitoExtension.class)
public class UnauthorizedUserBusinessExceptionUnitTest {
	
	@Test
	public void unauthorizedUser() {
		UnauthorizedUserBusinessException exception = new UnauthorizedUserBusinessException();
		
		assertEquals("bestmovies.user.unauthorizedUser", exception.getCode());
		assertEquals("Unauthorized user", exception.getMessage());
		assertEquals(HttpStatus.UNAUTHORIZED, exception.getHttpStatus());
	}

}
