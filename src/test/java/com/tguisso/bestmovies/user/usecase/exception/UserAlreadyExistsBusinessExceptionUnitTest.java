package com.tguisso.bestmovies.user.usecase.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

@ExtendWith(MockitoExtension.class)
public class UserAlreadyExistsBusinessExceptionUnitTest {

	@Test
	public void userExists() {
		UserAlreadyExistsBusinessException exception = new UserAlreadyExistsBusinessException();
		
		assertEquals("bestmovies.user.userAlreadyExists", exception.getCode());
		assertEquals("User already exists", exception.getMessage());
		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, exception.getHttpStatus());
	}

}
