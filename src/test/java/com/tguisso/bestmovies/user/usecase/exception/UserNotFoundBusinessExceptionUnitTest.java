package com.tguisso.bestmovies.user.usecase.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

@ExtendWith(MockitoExtension.class)
public class UserNotFoundBusinessExceptionUnitTest {
	
	@Test
	public void userNotFound() {
		UserNotFoundBusinessException exception = new UserNotFoundBusinessException();
		
		assertEquals("bestmovies.user.userNotFound", exception.getCode());
		assertEquals("User not found", exception.getMessage());
		assertEquals(HttpStatus.NOT_FOUND, exception.getHttpStatus());
	}

}
