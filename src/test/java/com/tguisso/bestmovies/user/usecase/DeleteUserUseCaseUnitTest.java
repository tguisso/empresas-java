package com.tguisso.bestmovies.user.usecase;

import static com.tguisso.bestmovies.test.databuilder.DataBuilderBase.getNextRandomString;
import static com.tguisso.bestmovies.test.databuilder.DataBuilderBase.getRandomLong;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.bestmovies.test.databuilder.domain.UserDataBuilder;
import com.tguisso.bestmovies.user.domain.User;
import com.tguisso.bestmovies.user.gateway.database.UserDBGateway;
import com.tguisso.bestmovies.user.usecase.exception.UnauthorizedUserBusinessException;
import com.tguisso.bestmovies.user.usecase.exception.UserNotFoundBusinessException;

@ExtendWith(MockitoExtension.class)
public class DeleteUserUseCaseUnitTest {
	
	@InjectMocks
	private DeleteUserUseCase deleteUserUseCase;
	
	@Mock
	private UserDBGateway userDBGateway;
	
	@Test
	public void deleteWithSuccess() {
		final User user = UserDataBuilder.getAnyUser();
		
		final Optional<User> userFound = Optional.of(user);
		doReturn(userFound).when(userDBGateway).findUserByUsername(user.getUsername());
		
		deleteUserUseCase.delete(user.getId(), user.getUsername());
		verify(userDBGateway).findUserByUsername(user.getUsername());
		verify(userDBGateway).deleteUserById(user.getId());
	}
	
	@Test
	public void deleteNotFoundException() {
		final Long id = getRandomLong();
		final String username = getNextRandomString();
		
		final Optional<User> userFound = Optional.empty();
		doReturn(userFound).when(userDBGateway).findUserByUsername(username);
		
		assertThrows(UserNotFoundBusinessException.class, () -> {
			deleteUserUseCase.delete(id, username);
		});
		
		verify(userDBGateway).findUserByUsername(username);
		verify(userDBGateway, never()).deleteUserById(id);
	}
	
	@Test
	public void deleteUnauthorizedUserException() {
		final User user = UserDataBuilder.getAnyAdmin();
		final Long id = getRandomLong();
		
		final Optional<User> userFound = Optional.of(user);
		doReturn(userFound).when(userDBGateway).findUserByUsername(user.getUsername());
		
		assertThrows(UnauthorizedUserBusinessException.class, () -> {
			deleteUserUseCase.delete(id, user.getUsername());
		});
		
		verify(userDBGateway).findUserByUsername(user.getUsername());
		verify(userDBGateway, never()).deleteUserById(id);
		
	}

}
