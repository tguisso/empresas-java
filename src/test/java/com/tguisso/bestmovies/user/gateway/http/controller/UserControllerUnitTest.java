package com.tguisso.bestmovies.user.gateway.http.controller;

import static com.tguisso.bestmovies.test.databuilder.DataBuilderBase.getNextRandomString;
import static com.tguisso.bestmovies.test.databuilder.DataBuilderBase.getRandomLong;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.springframework.test.util.ReflectionTestUtils.setField;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.bestmovies.test.databuilder.domain.UserDataBuilder;
import com.tguisso.bestmovies.test.databuilder.domain.UserJsonDataBuilder;
import com.tguisso.bestmovies.user.domain.User;
import com.tguisso.bestmovies.user.domain.UserType;
import com.tguisso.bestmovies.user.gateway.http.controller.json.UserJson;
import com.tguisso.bestmovies.user.usecase.CreateUpdateUserUseCase;
import com.tguisso.bestmovies.user.usecase.DeleteUserUseCase;
import com.tguisso.bestmovies.user.usecase.GetUserUseCase;

@ExtendWith(MockitoExtension.class)
public class UserControllerUnitTest {
	
	@InjectMocks
	private UserController userController;
	
	@Mock
	private CreateUpdateUserUseCase createUpdateUserUseCase;
	
	@Mock
	private DeleteUserUseCase deleteUserUseCase;
	
	@Mock
	private GetUserUseCase getUserUseCase;
	
	@Test
	public void createAdmin() {
		final UserJson userJson = UserJsonDataBuilder.getAnyAdmin();
		setField(userJson, "id", null);
		
		final Long idToReturn = getRandomLong();
		doReturn(idToReturn).when(createUpdateUserUseCase).create(any(User.class));
		
		final Long generatedId = userController.create(userJson); 
		
		ArgumentCaptor<User> userAC = ArgumentCaptor.forClass(User.class);
		verify(createUpdateUserUseCase).create(userAC.capture());
		
		final User user = userAC.getValue();
		
		assertEquals(idToReturn, generatedId);
		assertNull(user.getId());
		assertEquals(userJson.getName(), user.getName());
		assertEquals(userJson.getUsername(), user.getUsername());
		assertEquals(userJson.getPassword(), user.getPassword());
		assertEquals(userJson.getUserType(), user.getUserType());
		assertEquals(UserType.ADMIN, user.getUserType());
		assertNotNull(user.getVotes());
		assertTrue(user.getVotes().isEmpty());
		assertTrue(user.getActive());
	}
	
	@Test
	public void createCommonUser() {
		final UserJson userJson = UserJsonDataBuilder.getAnyUser();
		setField(userJson, "id", null);
		
		final Long idToReturn = getRandomLong();
		doReturn(idToReturn).when(createUpdateUserUseCase).create(any(User.class));
		
		final Long generatedId = userController.create(userJson); 
		
		ArgumentCaptor<User> userAC = ArgumentCaptor.forClass(User.class);
		verify(createUpdateUserUseCase).create(userAC.capture());
		
		final User user = userAC.getValue();
		
		assertEquals(idToReturn, generatedId);
		assertNull(user.getId());
		assertEquals(userJson.getName(), user.getName());
		assertEquals(userJson.getPassword(), user.getPassword());
		assertEquals(userJson.getUserType(), user.getUserType());
		assertTrue(user.getUserType() == UserType.USER);
	}
	
	@Test
	public void updateAdmin() {
		final UserJson userJson = UserJsonDataBuilder.getAnyAdmin();
		final Long id = getRandomLong();
		final String username = getNextRandomString();
		
		userController.update(id, username, userJson);
		
		ArgumentCaptor<User> userAC = ArgumentCaptor.forClass(User.class);
		verify(createUpdateUserUseCase).update(userAC.capture(), eq(username), eq(id));
		
		final User user = userAC.getValue();
		
		assertEquals(id, user.getId());
		assertEquals(userJson.getName(), user.getName());
		assertEquals(userJson.getPassword(), user.getPassword());
		assertEquals(userJson.getUserType(), user.getUserType());
		assertTrue(user.getUserType() == UserType.ADMIN);
		
	}
	
	@Test
	public void updateCommonUser() {
		final UserJson userJson = UserJsonDataBuilder.getAnyUser();
		final Long id = userJson.getId();
		final String username = getNextRandomString();
		
		userController.update(id, username, userJson);
		
		ArgumentCaptor<User> userAC = ArgumentCaptor.forClass(User.class);
		verify(createUpdateUserUseCase).update(userAC.capture(), eq(username), eq(id));
		
		final User user = userAC.getValue();
		
		assertEquals(id, user.getId());
		assertEquals(userJson.getName(), user.getName());
		assertEquals(userJson.getPassword(), user.getPassword());
		assertEquals(userJson.getUserType(), user.getUserType());
		assertTrue(user.getUserType() == UserType.USER);
		
	}
	
	@Test
	public void delete() {
		final Long id = getRandomLong();
		final String username = getNextRandomString();
		userController.delete(id, username);
		verify(deleteUserUseCase).delete(id, username);
	}
	
	@Test
	public void getAllActive() {
		final String username = getNextRandomString();
		
		final User userA = UserDataBuilder.getAnyUser();
		final User userB = UserDataBuilder.getAnyUser();
		final User userC = UserDataBuilder.getAnyUser();
		final User userD = UserDataBuilder.getAnyUser();
		final List<User> usersToReturn = Arrays.asList(userA, userB, userC, userD);
		doReturn(usersToReturn).when(getUserUseCase).getAllActive(username);
		
		final List<UserJson> usersJson = userController.getAllActive(username);
		
		verify(getUserUseCase).getAllActive(username);
		
		assertEquals(userA.getName(), usersJson.get(0).getName());
		
	}

}
