package com.tguisso.bestmovies.test.databuilder.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tguisso.bestmovies.movie.domain.Actor;
import com.tguisso.bestmovies.movie.domain.Director;
import com.tguisso.bestmovies.movie.domain.Gender;
import com.tguisso.bestmovies.movie.domain.Movie;
import com.tguisso.bestmovies.test.databuilder.DataBuilderBase;

public class MovieDataBuilder extends DataBuilderBase {
	
	public static Movie getAnyMovie() {		
		return Movie.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.votes(new ArrayList<>())
				.actors(getActors())
				.director(getDirector())
				.gender(Gender.ACTION)
				.build();
	}
	
	private static Director getDirector() {
		return Director.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();
	}

	private static List<Actor> getActors() {
		final Actor actor1 = Actor.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();		
		final Actor actor2 = Actor.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();	
		final Actor actor3 = Actor.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();
		return Arrays.asList(actor1, actor2, actor3);
	}
	

}
