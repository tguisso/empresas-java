package com.tguisso.bestmovies.test.databuilder.domain;

import com.tguisso.bestmovies.test.databuilder.DataBuilderBase;
import com.tguisso.bestmovies.user.domain.UserType;
import com.tguisso.bestmovies.user.gateway.http.controller.json.UserJson;

public class UserJsonDataBuilder extends DataBuilderBase {
	
	public static UserJson getAnyAdmin() {
		return UserJson.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.username(getNextRandomString())
				.password(getNextRandomString())
				.userType(UserType.ADMIN)
			.build();
	}
	
	public static UserJson getAnyUser() {
		return UserJson.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.username(getNextRandomString())
				.password(getNextRandomString())
				.userType(UserType.USER)
			.build();
	}

}
