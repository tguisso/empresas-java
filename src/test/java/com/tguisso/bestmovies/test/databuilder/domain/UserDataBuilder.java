package com.tguisso.bestmovies.test.databuilder.domain;

import com.tguisso.bestmovies.test.databuilder.DataBuilderBase;
import com.tguisso.bestmovies.user.domain.User;
import com.tguisso.bestmovies.user.domain.UserType;

public class UserDataBuilder extends DataBuilderBase {
	
	public static User getAnyAdmin() {
		return User.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.username(getNextRandomString())
				.password(getNextRandomString())
				.userType(UserType.ADMIN)
			.build();
	}
	
	public static User getAnyUser() {
		return User.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.username(getNextRandomString())
				.password(getNextRandomString())
				.userType(UserType.USER)
			.build();
	}
	
}
