package com.tguisso.bestmovies.test.databuilder.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tguisso.bestmovies.movie.domain.Gender;
import com.tguisso.bestmovies.movie.gateway.http.controller.json.ActorJson;
import com.tguisso.bestmovies.movie.gateway.http.controller.json.DirectorJson;
import com.tguisso.bestmovies.movie.gateway.http.controller.json.MovieJson;
import com.tguisso.bestmovies.test.databuilder.DataBuilderBase;

public class MovieJsonDataBuilder extends DataBuilderBase {
	
	public static MovieJson getAnyMovie() {		
		return MovieJson.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.votes(new ArrayList<>())
				.averageVote(0.0)
				.actors(getActorsJson())
				.director(getDirectorJson())
				.gender(Gender.ACTION)
				.build();
	}

	private static DirectorJson getDirectorJson() {
		return DirectorJson.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();
	}

	private static List<ActorJson> getActorsJson() {
		final ActorJson actorJson1 = ActorJson.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();		
		final ActorJson actorJson2 = ActorJson.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();	
		final ActorJson actorJson3 = ActorJson.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();
		return Arrays.asList(actorJson1, actorJson2, actorJson3);
	}
	
}
