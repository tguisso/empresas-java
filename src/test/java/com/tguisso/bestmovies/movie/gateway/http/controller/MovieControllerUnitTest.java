package com.tguisso.bestmovies.movie.gateway.http.controller;

import static com.tguisso.bestmovies.test.databuilder.DataBuilderBase.getNextRandomString;
import static com.tguisso.bestmovies.test.databuilder.DataBuilderBase.getRandomLong;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.springframework.test.util.ReflectionTestUtils.setField;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.bestmovies.movie.domain.Actor;
import com.tguisso.bestmovies.movie.domain.Director;
import com.tguisso.bestmovies.movie.domain.Gender;
import com.tguisso.bestmovies.movie.domain.Movie;
import com.tguisso.bestmovies.movie.domain.Vote;
import com.tguisso.bestmovies.movie.gateway.http.controller.json.MovieJson;
import com.tguisso.bestmovies.movie.usecase.CreateUpdateMovieUseCase;
import com.tguisso.bestmovies.movie.usecase.GetMovieUseCase;
import com.tguisso.bestmovies.test.databuilder.domain.MovieDataBuilder;
import com.tguisso.bestmovies.test.databuilder.domain.MovieJsonDataBuilder;

@ExtendWith(MockitoExtension.class)
public class MovieControllerUnitTest {
	
	@InjectMocks
	private MovieController movieController;
	
	@Mock
	private CreateUpdateMovieUseCase createUpdateMovieUseCase;
	
	@Mock
	private GetMovieUseCase getMovieUseCase;
	
	@Test
	public void create() {
		final String username = getNextRandomString();
		final MovieJson movieJson = MovieJsonDataBuilder.getAnyMovie();
		setField(movieJson, "id", null);
		
		final Long idToReturn = getRandomLong();
		doReturn(idToReturn).when(createUpdateMovieUseCase).save(any(Movie.class), eq(username));
		
		final Long generatedId = movieController.create(username, movieJson);
		
		final ArgumentCaptor<Movie> movieAC = ArgumentCaptor.forClass(Movie.class);
		verify(createUpdateMovieUseCase).save(movieAC.capture(), eq(username));
		
		final Movie movie = movieAC.getValue();
		
		assertEquals(idToReturn, generatedId);
		assertNull(movie.getId());
		assertEquals(movieJson.getName(), movie.getName());
		assertEquals(3, movie.getActors().size());
		assertEquals(movieJson.getActors().get(0).getId(), movie.getActors().get(0).getId());
		assertEquals(movieJson.getActors().get(1).getId(), movie.getActors().get(1).getId());
		assertEquals(movieJson.getActors().get(2).getId(), movie.getActors().get(2).getId());
		assertEquals(movieJson.getDirector().getId(), movie.getDirector().getId());
		assertEquals(movieJson.getGender(), movie.getGender());
		assertNotNull(movie.getVotes());
		assertTrue(movie.getVotes().isEmpty());
	}
	
	@Test
	public void update() {
		final Long id = getRandomLong();
		final String username = getNextRandomString();
		final MovieJson movieJson = MovieJsonDataBuilder.getAnyMovie();
		
		movieController.update(id, username, movieJson);
		
		ArgumentCaptor<Movie> movieAC = ArgumentCaptor.forClass(Movie.class);
		verify(createUpdateMovieUseCase).save(movieAC.capture(), eq(username));
		
		final Movie movie = movieAC.getValue();
		
		assertEquals(id, movie.getId());
		assertEquals(movieJson.getName(), movie.getName());
		assertEquals(movieJson.getActors().get(0).getId(), movie.getActors().get(0).getId());
		assertEquals(movieJson.getActors().get(1).getId(), movie.getActors().get(1).getId());
		assertEquals(movieJson.getActors().get(2).getId(), movie.getActors().get(2).getId());
		assertEquals(movieJson.getDirector().getId(), movie.getDirector().getId());
		assertEquals(movieJson.getDirector().getName(), movie.getDirector().getName());
		assertEquals(movieJson.getGender(), movie.getGender());
	}
	
	@Test
	public void getByName() {
		final String name = getNextRandomString();
		
		final List<Vote> votesMovieA = Arrays.asList(
				Vote.builder().vote(2).build(),
				Vote.builder().vote(0).build(),
				Vote.builder().vote(4).build(),
				Vote.builder().vote(1).build());
		final Movie movieA = MovieDataBuilder.getAnyMovie();
		setField(movieA, "votes", votesMovieA);
		setField(movieA, "name", name);

		final List<Vote> votesMovieB = Arrays.asList(
				Vote.builder().vote(0).build(),
				Vote.builder().vote(3).build());		
		final Movie movieB = MovieDataBuilder.getAnyMovie();
		setField(movieB, "votes", votesMovieB);
		setField(movieB, "name", name);
		
		final List<Movie> movies = Arrays.asList(movieA, movieB);
		doReturn(movies).when(getMovieUseCase).getByName(name);
		
		final List<MovieJson> moviesJson = movieController.getByName(name);
		
		verify(getMovieUseCase).getByName(name);
		
		assertEquals(2, moviesJson.size());
		assertEquals(movies.get(0).getId(), moviesJson.get(0).getId());
		assertEquals(movies.get(1).getId(), moviesJson.get(1).getId());
		assertEquals(movies.get(0).getName(), moviesJson.get(0).getName());
		assertEquals(movies.get(1).getName(), moviesJson.get(1).getName());
		assertEquals(4, moviesJson.get(0).getVotes().size());
		assertEquals(2, moviesJson.get(1).getVotes().size());
		assertEquals(3, moviesJson.get(0).getActors().size());
		assertEquals(3, moviesJson.get(1).getActors().size());
		assertEquals(movies.get(0).getDirector().getId(), moviesJson.get(0).getDirector().getId());
		assertEquals(movies.get(1).getDirector().getId(), moviesJson.get(1).getDirector().getId());
		assertEquals(movies.get(0).getGender(), moviesJson.get(0).getGender());
		assertEquals(movies.get(1).getGender(), moviesJson.get(1).getGender());
		assertEquals(1.75, moviesJson.get(0).getAverageVote());
		assertEquals(1.5, moviesJson.get(1).getAverageVote());
	}
	
	@Test
	public void getByGender() {
		final Gender gender = Gender.COMEDY;
		
		final Movie movieA = MovieDataBuilder.getAnyMovie();
		setField(movieA, "gender", gender);

		final Movie movieB = MovieDataBuilder.getAnyMovie();
		setField(movieB, "gender", gender);
		
		final List<Movie> movies = Arrays.asList(movieA, movieB);
		doReturn(movies).when(getMovieUseCase).getByGender(gender);
		
		final List<MovieJson> moviesJson = movieController.getByGender(gender);
		
		verify(getMovieUseCase).getByGender(gender);
		
		assertEquals(2, moviesJson.size());
		assertEquals(movies.get(0).getId(), moviesJson.get(0).getId());
		assertEquals(movies.get(1).getId(), moviesJson.get(1).getId());
		assertEquals(movies.get(0).getGender(), moviesJson.get(0).getGender());
		assertEquals(movies.get(1).getGender(), moviesJson.get(1).getGender());
	}
	
	@Test
	public void getByDirector() {
		final Director director = Director.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build(); 
		
		final Movie movieA = MovieDataBuilder.getAnyMovie();
		setField(movieA, "director", director);

		final Movie movieB = MovieDataBuilder.getAnyMovie();
		setField(movieB, "director", director);
		
		final List<Movie> movies = Arrays.asList(movieA, movieB);
		doReturn(movies).when(getMovieUseCase).getByDirector(director.getName());
		
		final List<MovieJson> moviesJson = movieController.getByDirector(director.getName());
		
		verify(getMovieUseCase).getByDirector(director.getName());
		
		assertEquals(2, moviesJson.size());
		assertEquals(movies.get(0).getId(), moviesJson.get(0).getId());
		assertEquals(movies.get(1).getId(), moviesJson.get(1).getId());
		assertEquals(movies.get(0).getDirector().getId(), moviesJson.get(0).getDirector().getId());
		assertEquals(movies.get(1).getDirector().getId(), moviesJson.get(1).getDirector().getId());
		assertEquals(movies.get(0).getDirector().getName(), moviesJson.get(0).getDirector().getName());
		assertEquals(movies.get(1).getDirector().getName(), moviesJson.get(1).getDirector().getName());
	}
	
	@Test
	public void getByActor() {
		final Actor actorA = Actor.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();
		final Actor actorB = Actor.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();
		final Actor actorC = Actor.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();
		final List<String> names = Arrays.asList(actorA.getName(), actorB.getName(), actorC.getName());
		final List<Actor> actorsMovieA = Arrays.asList(actorA, actorB);
		final List<Actor> actorsMovieB = Arrays.asList(actorB, actorC);
		
		
		final Movie movieA = MovieDataBuilder.getAnyMovie();
		setField(movieA, "actors", actorsMovieA);

		final Movie movieB = MovieDataBuilder.getAnyMovie();
		setField(movieB, "actors", actorsMovieB);
		
		final List<Movie> movies = Arrays.asList(movieA, movieB);
		doReturn(movies).when(getMovieUseCase).getByActors(names);
		
		final List<MovieJson> moviesJson = movieController.getByActors(names);
		
		verify(getMovieUseCase).getByActors(names);
		
		assertEquals(2, moviesJson.size());
		assertEquals(movies.get(0).getId(), moviesJson.get(0).getId());
		assertEquals(movies.get(1).getId(), moviesJson.get(1).getId());
		assertEquals(2, moviesJson.get(0).getActors().size());
		assertEquals(2, moviesJson.get(1).getActors().size());
		assertEquals(movies.get(0).getActors().get(0).getId(), moviesJson.get(0).getActors().get(0).getId());
		assertEquals(movies.get(0).getActors().get(1).getId(), moviesJson.get(0).getActors().get(1).getId());
		assertEquals(movies.get(1).getActors().get(0).getId(), moviesJson.get(1).getActors().get(0).getId());
		assertEquals(movies.get(1).getActors().get(1).getId(), moviesJson.get(1).getActors().get(1).getId());
		
	}

}
