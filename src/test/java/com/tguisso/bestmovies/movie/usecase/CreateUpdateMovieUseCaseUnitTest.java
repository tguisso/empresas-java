package com.tguisso.bestmovies.movie.usecase;

import static com.tguisso.bestmovies.test.databuilder.DataBuilderBase.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.springframework.test.util.ReflectionTestUtils.setField;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.bestmovies.movie.domain.Movie;
import com.tguisso.bestmovies.movie.gateway.database.MovieDBGateway;
import com.tguisso.bestmovies.test.databuilder.domain.MovieDataBuilder;
import com.tguisso.bestmovies.user.usecase.BaseUserUseCase;

@ExtendWith(MockitoExtension.class)
public class CreateUpdateMovieUseCaseUnitTest {
	
	@InjectMocks
	private CreateUpdateMovieUseCase createUpdateMovieUseCase;
	
	@Mock
	private BaseUserUseCase baseUserUseCase;
	
	@Mock
	private MovieDBGateway movieDBGateway;
	
	@Mock
	private GetMovieUseCase getMovieUseCase;
	
	@Test
	public void save() {
		final Movie movie = MovieDataBuilder.getAnyMovie();
		setField(movie, "id", null);
		final String username = getNextRandomString();
		
		final Long idToReturn = getRandomLong();
		doReturn(idToReturn).when(movieDBGateway).save(movie);
		
		final Long generatedId = createUpdateMovieUseCase.save(movie, username);
		
		verify(movieDBGateway).save(movie);
		verify(baseUserUseCase).checkIfIsAdmin(username);
		
		assertEquals(idToReturn, generatedId);
		
	}

}
