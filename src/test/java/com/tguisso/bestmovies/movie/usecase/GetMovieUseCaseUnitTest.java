package com.tguisso.bestmovies.movie.usecase;

import static com.tguisso.bestmovies.test.databuilder.DataBuilderBase.getNextRandomString;
import static com.tguisso.bestmovies.test.databuilder.DataBuilderBase.getRandomLong;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.springframework.test.util.ReflectionTestUtils.setField;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tguisso.bestmovies.movie.domain.Actor;
import com.tguisso.bestmovies.movie.domain.Director;
import com.tguisso.bestmovies.movie.domain.Gender;
import com.tguisso.bestmovies.movie.domain.Movie;
import com.tguisso.bestmovies.movie.domain.Vote;
import com.tguisso.bestmovies.movie.gateway.database.MovieDBGateway;
import com.tguisso.bestmovies.test.databuilder.domain.MovieDataBuilder;

@ExtendWith(MockitoExtension.class)
public class GetMovieUseCaseUnitTest {
	
	@InjectMocks
	private GetMovieUseCase getMovieUseCase;
	
	@Mock
	private MovieDBGateway movieDBGateway;
	
	@Test
	public void getByName() {
		final String name = getNextRandomString();
		
		final List<Vote> votesMovieA = Arrays.asList(
				Vote.builder().vote(2).build(),
				Vote.builder().vote(0).build(),
				Vote.builder().vote(4).build(),
				Vote.builder().vote(1).build());
		final Movie movieA = MovieDataBuilder.getAnyMovie();
		setField(movieA, "votes", votesMovieA);
		setField(movieA, "name", name);

		final List<Vote> votesMovieB = Arrays.asList(
				Vote.builder().vote(0).build(),
				Vote.builder().vote(3).build());		
		final Movie movieB = MovieDataBuilder.getAnyMovie();
		setField(movieB, "votes", votesMovieB);
		setField(movieB, "name", name);
		
		final List<Movie> moviesToReturn = Arrays.asList(movieA, movieB);
		doReturn(moviesToReturn).when(movieDBGateway).findByName(name);
		
		final List<Movie> movies = getMovieUseCase.getByName(name);
		
		verify(movieDBGateway).findByName(name);
		
		assertEquals(2, movies.size());
		assertEquals(movieA.getName(), movies.get(0).getName());
		assertEquals(movieB.getName(), movies.get(1).getName());
		assertEquals(movieA.getId(), movies.get(0).getId());
		assertEquals(movieB.getId(), movies.get(1).getId());
		assertEquals(movieA.getVotes().size(), movies.get(0).getVotes().size());
		assertEquals(movieB.getVotes().size(), movies.get(1).getVotes().size());
		assertEquals(movieA.getActors().size(), movies.get(0).getActors().size());
		assertEquals(movieB.getActors().size(), movies.get(1).getActors().size());
		assertEquals(movieA.getDirector().getId(), movies.get(0).getDirector().getId());
		assertEquals(movieB.getDirector().getId(), movies.get(1).getDirector().getId());
		assertEquals(movieA.getGender(), movies.get(0).getGender());
		assertEquals(movieB.getGender(), movies.get(1).getGender());
	}
	
	@Test
	public void getByGender() {
		final Gender gender = Gender.COMEDY;
		
		final Movie movieA = MovieDataBuilder.getAnyMovie();
		setField(movieA, "gender", gender);

		final Movie movieB = MovieDataBuilder.getAnyMovie();
		setField(movieB, "gender", gender);
		
		final List<Movie> moviesToReturn = Arrays.asList(movieA, movieB);
		doReturn(moviesToReturn).when(movieDBGateway).findByGender(gender);
		
		final List<Movie> movies = getMovieUseCase.getByGender(gender);
		
		verify(movieDBGateway).findByGender(gender);
		
		assertEquals(2, movies.size());
		assertEquals(movieA.getId(), movies.get(0).getId());
		assertEquals(movieB.getId(), movies.get(1).getId());
		assertEquals(movieA.getGender(), movies.get(0).getGender());
		assertEquals(movieB.getGender(), movies.get(1).getGender());
	}
	
	@Test
	public void getByDirector() {
		final Director director = Director.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build(); 
		
		final Movie movieA = MovieDataBuilder.getAnyMovie();
		setField(movieA, "director", director);

		final Movie movieB = MovieDataBuilder.getAnyMovie();
		setField(movieB, "director", director);
		
		final List<Movie> moviesToReturn = Arrays.asList(movieA, movieB);
		doReturn(moviesToReturn).when(movieDBGateway).findByDirector(director.getName());
		
		final List<Movie> movies = getMovieUseCase.getByDirector(director.getName());
		
		verify(movieDBGateway).findByDirector(director.getName());
		
		assertEquals(2, movies.size());
		assertEquals(movieA.getId(), movies.get(0).getId());
		assertEquals(movieB.getId(), movies.get(1).getId());
		assertEquals(movieA.getDirector().getId(), movies.get(0).getDirector().getId());
		assertEquals(movieB.getDirector().getId(), movies.get(1).getDirector().getId());
	}
	
	@Test
	public void getByActor() {
		final Actor actorA = Actor.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();
		final Actor actorB = Actor.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();
		final Actor actorC = Actor.builder()
				.id(getRandomLong())
				.name(getNextRandomString())
				.build();
		final List<String> names = Arrays.asList(actorA.getName(), actorB.getName(), actorC.getName());
		final List<Actor> actorsMovieA = Arrays.asList(actorA, actorB);
		final List<Actor> actorsMovieB = Arrays.asList(actorB, actorC);
		
		final Movie movieA = MovieDataBuilder.getAnyMovie();
		setField(movieA, "actors", actorsMovieA);

		final Movie movieB = MovieDataBuilder.getAnyMovie();
		setField(movieB, "actors", actorsMovieB);
		
		final List<Movie> moviesToReturn = Arrays.asList(movieA, movieB);
		doReturn(moviesToReturn).when(movieDBGateway).findByActors(names);
		
		final List<Movie> movies = getMovieUseCase.getByActors(names);
		
		verify(movieDBGateway).findByActors(names);
		
		assertEquals(2, movies.size());
		assertEquals(movieA.getId(), movies.get(0).getId());
		assertEquals(movieB.getId(), movies.get(1).getId());
		assertEquals(movieA.getActors().get(0).getId(), movies.get(0).getActors().get(0).getId());
		assertEquals(movieA.getActors().get(1).getId(), movies.get(0).getActors().get(1).getId());
		assertEquals(movieB.getActors().get(0).getId(), movies.get(1).getActors().get(0).getId());
		assertEquals(movieB.getActors().get(1).getId(), movies.get(1).getActors().get(1).getId());
	}

}
