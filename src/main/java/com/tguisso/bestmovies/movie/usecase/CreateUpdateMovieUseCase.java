package com.tguisso.bestmovies.movie.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.bestmovies.movie.domain.Movie;
import com.tguisso.bestmovies.movie.gateway.database.MovieDBGateway;
import com.tguisso.bestmovies.user.usecase.BaseUserUseCase;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CreateUpdateMovieUseCase {
	
	@Autowired
	private BaseUserUseCase baseUserUseCase;
	
	@Autowired
	private MovieDBGateway movieDBGateway;
	
	public Long save(Movie movie, String username) {
		log.trace("Start movie={}, username={}", movie, username);
		baseUserUseCase.checkIfIsAdmin(username);
		final Long generatedId = movieDBGateway.save(movie);
		log.trace("End generatedId={}", generatedId);
		return generatedId;
	}
	
}
