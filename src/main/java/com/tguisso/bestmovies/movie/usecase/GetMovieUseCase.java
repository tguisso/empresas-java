package com.tguisso.bestmovies.movie.usecase;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.bestmovies.movie.domain.Gender;
import com.tguisso.bestmovies.movie.domain.Movie;
import com.tguisso.bestmovies.movie.gateway.database.MovieDBGateway;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GetMovieUseCase {
	
	@Autowired
	private MovieDBGateway movieDBGateway;

	public List<Movie> getByName(String name) {
		log.trace("Start name={}", name);
		List<Movie> movies = movieDBGateway.findByName(name);
		log.trace("End movies={}", movies);
		return movies;
	}

	public List<Movie> getByGender(Gender gender) {
		log.trace("Start gender={}", gender);
		List<Movie> movies = movieDBGateway.findByGender(gender);
		log.trace("End movies={}", movies);
		return movies;
	}

	public List<Movie> getByDirector(String name) {
		log.trace("Start name={}", name);
		List<Movie> movies = movieDBGateway.findByDirector(name);
		log.trace("End movies={}", movies);
		return movies;
	}
	
	public List<Movie> getByActors(List<String> names) {
		log.trace("Start names={}", names);
		List<Movie> movies = movieDBGateway.findByActors(names);
		log.trace("End movies={}", movies);
		return movies;
	}

}
