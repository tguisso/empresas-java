package com.tguisso.bestmovies.movie.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.bestmovies.movie.domain.Vote;
import com.tguisso.bestmovies.movie.gateway.database.MovieDBGateway;
import com.tguisso.bestmovies.movie.usecase.exception.InvalidVoteException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class VoteUseCase {

	@Autowired
	private MovieDBGateway movieDBGateway;
	
	public void vote(Vote voteDomain) {
		log.trace("Start voteDomain={}", voteDomain);
		
		if(voteDomain.getVote() < 0 || voteDomain.getVote() > 4) {
			log.warn("Invalid vote");
			throw new InvalidVoteException();
		}
		
		movieDBGateway.vote(voteDomain);
		log.trace("End");
	}

}
