package com.tguisso.bestmovies.movie.usecase.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class InvalidVoteException extends RuntimeException {
	private static final long serialVersionUID = -8089942744954218305L;

	private String code = "bestmovies.movie.invalidVote";
	private String message = "Invalid vote: must be between 0 and 4";
	private HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
}
