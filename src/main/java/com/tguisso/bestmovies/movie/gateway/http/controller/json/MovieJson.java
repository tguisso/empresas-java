package com.tguisso.bestmovies.movie.gateway.http.controller.json;

import java.util.List;

import com.tguisso.bestmovies.movie.domain.Gender;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class MovieJson {
	private Long id;
	private String name;
	private List<VoteJson> votes;
	private List<ActorJson> actors;
	private DirectorJson director;
	private Gender gender;

	private Double averageVote;
	
}
