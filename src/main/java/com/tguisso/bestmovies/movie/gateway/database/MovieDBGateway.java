package com.tguisso.bestmovies.movie.gateway.database;

import java.util.List;

import com.tguisso.bestmovies.movie.domain.Gender;
import com.tguisso.bestmovies.movie.domain.Movie;
import com.tguisso.bestmovies.movie.domain.Vote;

public interface MovieDBGateway {

	Long save(Movie movie);

	List<Movie> findByName(String name);

	List<Movie> findByGender(Gender gender);

	List<Movie> findByDirector(String name);

	List<Movie> findByActors(List<String> names);

	void vote(Vote voteDomain);

}
