package com.tguisso.bestmovies.movie.gateway.database.db;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tguisso.bestmovies.movie.domain.Gender;
import com.tguisso.bestmovies.movie.domain.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long> {

	List<Movie> findByName(String name);

	List<Movie> findByGender(Gender gender);

	List<Movie> findByDirectorName(String name);

	List<Movie> findByActorsNameIn(List<String> names);

}
