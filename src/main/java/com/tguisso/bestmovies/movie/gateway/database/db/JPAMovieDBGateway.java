package com.tguisso.bestmovies.movie.gateway.database.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tguisso.bestmovies.movie.domain.Actor;
import com.tguisso.bestmovies.movie.domain.Director;
import com.tguisso.bestmovies.movie.domain.Gender;
import com.tguisso.bestmovies.movie.domain.Movie;
import com.tguisso.bestmovies.movie.domain.Vote;
import com.tguisso.bestmovies.movie.domain.VotePK;
import com.tguisso.bestmovies.movie.gateway.database.MovieDBGateway;
import com.tguisso.bestmovies.movie.gateway.database.exception.ErrorToAccessDatabaseGatewayException;
import com.tguisso.bestmovies.user.domain.User;
import com.tguisso.bestmovies.user.gateway.database.db.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class JPAMovieDBGateway implements MovieDBGateway {
	
	@Autowired
	private MovieRepository movieRepository;

	@Autowired
	private DirectorRepository directorRepository;

	@Autowired
	private ActorRepository actorRepository;
	
	@Autowired
	private VoteRepository voteRepository;

	@Autowired
	private UserRepository userRepository;

	
	@Override
	public Long save(Movie movie) {
		log.trace("Start movie={}", movie);
		try {
			getDirector(movie);
			getActors(movie);
			
			final Long generatedId = movieRepository.save(movie).getId();
			log.trace("End generatedId={}", generatedId);
			return generatedId;

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public List<Movie> findByName(String name) {
		log.trace("Start name={}", name);
		try {
			final List<Movie> movies = movieRepository.findByName(name);
			log.trace("End movies={}", movies);
			return movies;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public List<Movie> findByGender(Gender gender) {
		log.trace("Start gender={}", gender);
		try {
			final List<Movie> movies = movieRepository.findByGender(gender);
			log.trace("End movies={}", movies);
			return movies;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public List<Movie> findByDirector(String name) {
		log.trace("Start name={}", name);
		try {
			final List<Movie> movies = movieRepository.findByDirectorName(name);
			log.trace("End movies={}", movies);
			return movies;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public List<Movie> findByActors(List<String> names) {
		log.trace("Start names={}", names);
		try {
			final List<Movie> movies = movieRepository.findByActorsNameIn(names);
			log.trace("End movies={}", movies);
			return movies;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public void vote(final Vote voteDomain) {
		log.trace("Start voteDomain={}", voteDomain);
		try {
			
			final Optional<Movie> movieToVoteOP = movieRepository.findById(voteDomain.getMovie().getId()); 
			final Optional<User> userToVoteOP = userRepository.findById(voteDomain.getUser().getId());
			
			final VotePK votePK = new VotePK(voteDomain.getMovie().getId(), voteDomain.getUser().getId());
			
			final User userToVote = userToVoteOP.get();
			final Movie movieToVote = movieToVoteOP.get();
			
			//TODO: lançar exceptio caso os optional esteja vazio
			
			final Vote voteToSave = Vote.builder()
				.votePK(votePK)
				.user(userToVote)
				.movie(movieToVote)
				.vote(voteDomain.getVote())
			.build();
			
			voteRepository.save(voteToSave);
			
			log.trace("End");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	
	private void getActors(Movie movie) {
		if(movie.getActors() != null) {
			final List<Actor> actorsAtached = new ArrayList<>();
			
			for (Actor actor : movie.getActors()) {
				if(actor.getId() == null) {
					actorsAtached.add(actor);
				} else {
					actorsAtached.add(actorRepository.getOne(actor.getId()));
				}
			}
			movie.setActors(actorsAtached);
		}
	}

	private void getDirector(Movie movie) {
		final Director director = movie.getDirector();
		if(director.getId() != null) {
			
			final Optional<Director> directorOP = directorRepository.findById(movie.getDirector().getId());
			if(directorOP.isPresent()) {
				movie.setDirector(directorOP.get());
			}else {
				director.setId(null);
			}
		}
	}


}
