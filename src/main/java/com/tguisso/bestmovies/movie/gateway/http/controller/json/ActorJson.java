package com.tguisso.bestmovies.movie.gateway.http.controller.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class ActorJson {
	private Long id;
	private String name;
}
