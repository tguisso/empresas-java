package com.tguisso.bestmovies.movie.gateway.database.db;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tguisso.bestmovies.movie.domain.Vote;

public interface VoteRepository extends JpaRepository<Vote, Long> {

}
