package com.tguisso.bestmovies.movie.gateway.http.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import com.tguisso.bestmovies.movie.domain.Actor;
import com.tguisso.bestmovies.movie.domain.Director;
import com.tguisso.bestmovies.movie.domain.Gender;
import com.tguisso.bestmovies.movie.domain.Movie;
import com.tguisso.bestmovies.movie.domain.Vote;
import com.tguisso.bestmovies.movie.gateway.http.controller.json.ActorJson;
import com.tguisso.bestmovies.movie.gateway.http.controller.json.DirectorJson;
import com.tguisso.bestmovies.movie.gateway.http.controller.json.MovieJson;
import com.tguisso.bestmovies.movie.gateway.http.controller.json.VoteJson;
import com.tguisso.bestmovies.movie.usecase.CreateUpdateMovieUseCase;
import com.tguisso.bestmovies.movie.usecase.GetMovieUseCase;
import com.tguisso.bestmovies.movie.usecase.VoteUseCase;
import com.tguisso.bestmovies.user.domain.User;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("movies")
@Api(tags = "Movies", produces = MediaType.APPLICATION_JSON_VALUE)
public class MovieController {

	@Autowired
	private CreateUpdateMovieUseCase createUpdateMovieUseCase;

	@Autowired
	private GetMovieUseCase getMovieUseCase;
	
	@Autowired
	private VoteUseCase voteUseCase;
	
	@ApiOperation(value = "Create movie", response = Long.class, responseContainer = "Created movie id")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Create with sucess"), 
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 422, message = "Unprocessable Entity"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Long create(
			@RequestHeader(name = "username", required = true) final String username,
			@RequestBody(required = true) final MovieJson movieJson) {
		log.trace("Start movieJson={}", movieJson);
		final Movie movie = mapperJsonToMovie(movieJson, null);
		final Long generatedId = createUpdateMovieUseCase.save(movie, username);
		log.trace("End generatedId={}", generatedId);
		return generatedId;
	}

	@ApiOperation(value = "Update movie")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Updated with sucess"), 
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 422, message = "Unprocessable Entity"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	
	@PutMapping("{id}")
	public void update(
			@PathVariable(name = "id", required = true) final Long id,
			@RequestHeader(name = "username", required = true) final String username,
			@RequestBody(required = true) final MovieJson movieJson) {
		log.trace("Start username={}, movieJson={}", username, movieJson);
		final Movie movie = mapperJsonToMovie(movieJson, id);
		createUpdateMovieUseCase.save(movie, username);
		log.trace("End");
	}

	@ApiOperation(value = "Get movie by name", response = List.class, responseContainer = "Movie list filtered by name")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Movies found"), 
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 422, message = "Unprocessable Entity"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	@GetMapping("name/{name}")
	public List<MovieJson> getByName(
			@PathVariable(name = "name", required = true) final String name) {
		log.trace("Start name={}", name);
		List<Movie> movies = getMovieUseCase.getByName(name);
		List<MovieJson> moviesJson = mapperMovieToJson(movies);
		log.trace("End moviesJson={}", moviesJson);
		return moviesJson;
	}

	@ApiOperation(value = "Get movie by gender", response = List.class, responseContainer = "Movie list filtered by gender")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Movies found"), 
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 422, message = "Unprocessable Entity"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	@GetMapping("gender/{gender}")
	public List<MovieJson> getByGender(
			@PathVariable(name = "gender", required = true) final Gender gender) {
		List<Movie> movies = getMovieUseCase.getByGender(gender);
		List<MovieJson> moviesJson = mapperMovieToJson(movies);
		log.trace("End moviesJson={}", moviesJson);
		return moviesJson;
	}
	
	@ApiOperation(value = "Get movie by director", response = List.class, responseContainer = "Movie list filtered by director")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Movies found"), 
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 422, message = "Unprocessable Entity"),
			@ApiResponse(code = 500, message = "Internal Server Error") })

	@GetMapping("directors/name/{name}")
	public List<MovieJson> getByDirector(
			@PathVariable(name = "name", required = true) final String name) {
		log.trace("Start name={}", name);
		List<Movie> movies = getMovieUseCase.getByDirector(name);
		List<MovieJson> moviesJson = mapperMovieToJson(movies);
		log.trace("End moviesJson={}", moviesJson);
		return moviesJson;
	}
	
	@ApiOperation(value = "Get movie by actors", response = List.class, responseContainer = "Movie list filtered by actors")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Movies found"), 
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 422, message = "Unprocessable Entity"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	@GetMapping("actors")
	public List<MovieJson> getByActors(
			@RequestParam(name = "name", required = true) final List<String> names) {
		log.trace("Start names={}", names);
		List<Movie> movies = getMovieUseCase.getByActors(names);
		List<MovieJson> moviesJson = mapperMovieToJson(movies);
		log.trace("End moviesJson={}", moviesJson);
		return moviesJson;
	}

	
	//TODO: criar unitTest
	@PostMapping("{movieId}/vote")
	public void vote(
			@PathVariable(name = "movieId", required = true) final Long movieId,
			@RequestBody(required = true) final VoteJson vote) {
		
		log.trace("Start movieId={}, vote={}", movieId, vote);
		
		final Vote voteDomain = Vote.builder()
				.user(User.builder().id(vote.getUserId()).build())
				.movie(Movie.builder().id(movieId).build())
				.vote(vote.getVote())
				.build();
		
		voteUseCase.vote(voteDomain);
		
		log.trace("End");
	}
	
	private List<MovieJson> mapperMovieToJson(List<Movie> movies) {
		List<MovieJson> moviesJson = new ArrayList<>();
		for (Movie movie : movies) {
			final List<ActorJson> actorsJson = new ArrayList<>();
			for (Actor actor : movie.getActors()) {
				final ActorJson actorJson = ActorJson.builder()
						.id(actor.getId())
						.name(actor.getName())
						.build();
				actorsJson.add(actorJson);
			}

			final DirectorJson directorJson = DirectorJson.builder()
					.id(movie.getDirector().getId())
					.name(movie.getDirector().getName())
					.build();
			
			final List<VoteJson> votesJson = new ArrayList<>();
			for (Vote vote : movie.getVotes()) {
				final VoteJson voteJson = VoteJson.builder()
						.vote(vote.getVote())
						.build();
				votesJson.add(voteJson);
			}

			final MovieJson movieJson = MovieJson.builder()
					.id(movie.getId())
					.name(movie.getName())
					.votes(votesJson)
					.averageVote(movie.getAverageVote())
					.actors(actorsJson)
					.director(directorJson)
					.gender(movie.getGender())
					.build();
			moviesJson.add(movieJson);
		}
		return moviesJson;
	}
	
	private Movie mapperJsonToMovie(final MovieJson movieJson, final Long id) {
		final List<Actor> actors = new ArrayList<>();
		for (ActorJson actorJson : movieJson.getActors()) {
			final Actor actor = Actor.builder()
					.id(actorJson.getId())
					.name(actorJson.getName())
					.build(); 
			actors.add(actor);
		}
		final Director director = Director.builder()
				.id(movieJson.getDirector().getId())
				.name(movieJson.getDirector().getName())
				.movies(new ArrayList<>())
				.build();

		return Movie.builder()
				.id(id)
				.name(movieJson.getName())
				.actors(actors)
				.director(director)
				.votes(new ArrayList<>())
				.gender(movieJson.getGender())
				.build();
	}

}
