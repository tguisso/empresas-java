package com.tguisso.bestmovies.movie.gateway.database.db;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tguisso.bestmovies.movie.domain.Director;

public interface DirectorRepository extends JpaRepository<Director, Long> {

}
