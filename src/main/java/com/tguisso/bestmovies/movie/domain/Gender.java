package com.tguisso.bestmovies.movie.domain;

public enum Gender {
	ACTION,
	ANIMATION,
	ADVENTURE,
	COMEDY,
	DOCUMENTARY,
	WESTERN,
	WAR,
	SCI_FI,
	MUSICAL,
	ROMANCE,
	THRILLER,
	HORROR,
	DRAMA;

}
