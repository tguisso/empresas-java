package com.tguisso.bestmovies.movie.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VotePK implements Serializable {

	private static final long serialVersionUID = -7692668014683338101L;

	//TODO debito tecnico: colunas duplicadas na criação do banco.
	private Long movieId;
	private Long userId;

}
