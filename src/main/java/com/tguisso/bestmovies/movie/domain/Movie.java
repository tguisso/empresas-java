package com.tguisso.bestmovies.movie.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Builder
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Movie {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, length = 150)
	private String name;
	
	@OneToMany(mappedBy = "movie")
	private List<Vote> votes;
	
	@Setter
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="MovieActor",
	joinColumns={@JoinColumn(name="movie_id")},
	inverseJoinColumns={@JoinColumn(name="actor_id")})
	private List<Actor> actors;
	
	@Setter
	@ManyToOne(cascade = CascadeType.ALL)
	private Director director;
	
	@Enumerated(EnumType.ORDINAL)
	private Gender gender;
	
	public Double getAverageVote() {
		int quantity = 0;
		int totalVotes = 0;
		for (Vote vote : votes) {
			totalVotes += vote.getVote();
			quantity++;
		}
		return ((double)totalVotes / (double)quantity);
	}
}
