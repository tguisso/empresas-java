package com.tguisso.bestmovies.movie.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.tguisso.bestmovies.user.domain.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Getter
@ToString
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Vote {
	
	@EmbeddedId
	private VotePK votePK;
	
	@Column(nullable = false)
	private Integer vote;
	
	@ManyToOne
	private Movie movie;
	
	@ManyToOne
	private User user; 
}
