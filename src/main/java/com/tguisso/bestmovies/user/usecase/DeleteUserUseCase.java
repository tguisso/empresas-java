package com.tguisso.bestmovies.user.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.bestmovies.user.gateway.database.UserDBGateway;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DeleteUserUseCase extends BaseUserUseCase {
	
	@Autowired
	private UserDBGateway userDBGateway;
	
	public void delete(final Long id, final String username) {
		log.trace("Start id={}", id);
		checkAuthorizationToModify(username, id);
		userDBGateway.deleteUserById(id);
		log.trace("End");
	}

}
