package com.tguisso.bestmovies.user.usecase.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class UnauthorizedUserBusinessException extends RuntimeException {
	private static final long serialVersionUID = -4147038676828177158L;

		private String code = "bestmovies.user.unauthorizedUser";
		private String message = "Unauthorized user";
		private HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;

}
