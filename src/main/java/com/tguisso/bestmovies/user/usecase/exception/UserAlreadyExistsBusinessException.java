package com.tguisso.bestmovies.user.usecase.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class UserAlreadyExistsBusinessException extends RuntimeException {
	private static final long serialVersionUID = 309796538216866014L;
	
	private String code = "bestmovies.user.userAlreadyExists";
	private String message = "User already exists";
	private HttpStatus httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
	

}
