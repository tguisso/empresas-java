package com.tguisso.bestmovies.user.usecase;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.bestmovies.user.domain.User;
import com.tguisso.bestmovies.user.gateway.database.UserDBGateway;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GetUserUseCase extends BaseUserUseCase {
	
	@Autowired
	private UserDBGateway userDBGateway;

	public List<User> getAllActive(String username) {
		log.trace("Start username={}", username);
		checkIfIsAdmin(username);
		final List<User> users = userDBGateway.findAllActiveUser();
		log.trace("End users={}", users);
		return users;
	}

}
