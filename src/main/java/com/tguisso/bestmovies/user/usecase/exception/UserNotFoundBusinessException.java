package com.tguisso.bestmovies.user.usecase.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class UserNotFoundBusinessException extends RuntimeException {
	private static final long serialVersionUID = -2665950214785928771L;
	
	private String code = "bestmovies.user.userNotFound";
	private String message = "User not found";
	private HttpStatus httpStatus = HttpStatus.NOT_FOUND;
}
