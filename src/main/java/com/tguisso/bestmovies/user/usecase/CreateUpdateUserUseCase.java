package com.tguisso.bestmovies.user.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.bestmovies.user.domain.User;
import com.tguisso.bestmovies.user.gateway.database.UserDBGateway;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CreateUpdateUserUseCase extends BaseUserUseCase {
	
	@Autowired
	private UserDBGateway userDBGateway;

	public Long create(User user) {
		log.trace("Start user={}", user);
		validateToCreate(user);
		final Long userId = userDBGateway.save(user);
		log.trace("End userId={}", userId);
		return userId;
	}

	public void update(User user, String username, Long id) {
		log.trace("Start user={}, username={}", user, username);
		checkAuthorizationToModify(username, id);
		userDBGateway.save(user);
		log.trace("End");
	}

}
