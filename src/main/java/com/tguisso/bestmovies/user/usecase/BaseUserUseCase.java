package com.tguisso.bestmovies.user.usecase;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tguisso.bestmovies.user.domain.User;
import com.tguisso.bestmovies.user.domain.UserType;
import com.tguisso.bestmovies.user.gateway.database.UserDBGateway;
import com.tguisso.bestmovies.user.usecase.exception.UnauthorizedUserBusinessException;
import com.tguisso.bestmovies.user.usecase.exception.UserAlreadyExistsBusinessException;
import com.tguisso.bestmovies.user.usecase.exception.UserNotFoundBusinessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BaseUserUseCase {
	
	@Autowired
	private UserDBGateway userDBGateway;
	
	public void validateToCreate(User user) {
		log.trace("Start user={}", user);
		final Optional<User> userFound = getUser(user.getUsername());
		if(userFound.isPresent()) {
			log.warn("User already exists");
			throw new UserAlreadyExistsBusinessException();
		}
		log.trace("End");
	}
	
	public void checkAuthorizationToModify(String username, Long id) {
		log.trace("Start username={}, id={}", username, id);
		final Optional<User> userFound = getUser(username);
		checkUserExistence(userFound);
		if(!userFound.get().getId().equals(id)) {
			log.warn("Unauthorized user");
			throw new UnauthorizedUserBusinessException();
		}
	}
	
	public void checkIfIsAdmin(String username) {
		final Optional<User> userFound = getUser(username);
		checkUserExistence(userFound);
		if(userFound.get().getUserType().equals(UserType.USER)) {
			log.warn("Unauthorized user");
			throw new UnauthorizedUserBusinessException();
		}		
	}
	
	private void checkUserExistence(final Optional<User> userFound) {
		if(userFound.isEmpty()) {
			log.warn("User not found");
			throw new UserNotFoundBusinessException();
		}
	}

	private Optional<User> getUser(String username) {
		return userDBGateway.findUserByUsername(username);
	}
	
}
