package com.tguisso.bestmovies.user.domain;

public enum UserType {
	ADMIN,
	USER;
}
