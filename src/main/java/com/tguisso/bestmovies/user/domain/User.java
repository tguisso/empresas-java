package com.tguisso.bestmovies.user.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.tguisso.bestmovies.movie.domain.Vote;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Getter
@ToString(exclude = "password")
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, length = 150)
	private String name;
	
	@Column(nullable = false, length = 50)
	private String username;
	
	@Column(nullable = false, length = 8)
	private String password;
	
	@OneToMany(mappedBy = "user")
	private List<Vote> votes;
	
	@Column(nullable = false)
	private UserType userType;
	
	@Column(nullable = false)
	private Boolean active;
}
