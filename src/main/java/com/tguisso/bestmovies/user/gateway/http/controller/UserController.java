package com.tguisso.bestmovies.user.gateway.http.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tguisso.bestmovies.user.domain.User;
import com.tguisso.bestmovies.user.gateway.http.controller.json.UserJson;
import com.tguisso.bestmovies.user.usecase.CreateUpdateUserUseCase;
import com.tguisso.bestmovies.user.usecase.DeleteUserUseCase;
import com.tguisso.bestmovies.user.usecase.GetUserUseCase;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("users")
@Api(tags = "User", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {
	
	@Autowired
	private CreateUpdateUserUseCase createUpdateUserUseCase;
	
	@Autowired
	private DeleteUserUseCase deleteUserUseCase;
	
	@Autowired
	private GetUserUseCase getUserUseCase;
	
	@ApiOperation(value = "Create user", response = Long.class, responseContainer = "Created user id")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Create with sucess"), 
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 422, message = "Bussines errors"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Long create(@RequestBody(required = true) final UserJson userJson) {
		log.trace("Start userJson={}", userJson);
		final User user = mapperUserJsonToUser(userJson, null);
		final Long generatedId = createUpdateUserUseCase.create(user);
		log.trace("End generatedId={}", generatedId);
		return generatedId;
	}

	@ApiOperation(value = "Update a user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Updated with sucess"), 
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 422, message = "Bussines errors"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	@PutMapping("{id}")
	public void update(
			@PathVariable(name = "id", required = true) final Long id,
			@RequestHeader(name = "username", required = true) final String username,
			@RequestBody(required = true) final UserJson userJson) {
		log.trace("Start id={}, username={}, userJson={}", id, username, userJson);
		final User user = mapperUserJsonToUser(userJson, id);
		createUpdateUserUseCase.update(user, username, id);
		log.trace("End");
	}
	
	@ApiOperation(value = "Delete user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Deleted with sucess"), 
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 422, message = "Bussines errors"),
			@ApiResponse(code = 500, message = "Internal Server Error") })

	@DeleteMapping("{id}")
	//TODO analisar a utilização do Delete ou Patch. Pois será uma exclusão lógica. 
	public void delete(
			@PathVariable(name = "id", required = true) final Long id,
			@RequestHeader(name = "username", required = true) final String username) {
		log.trace("Start id={}, username={}", id, username);
		deleteUserUseCase.delete(id, username);
		log.trace("End");
	}

	@ApiOperation(value = "Get all active user", response = List.class, responseContainer = "Active users")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Get active users with sucess"), 
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 422, message = "Bussines errors"),
			@ApiResponse(code = 500, message = "Internal Server Error") })

	@GetMapping("actives")
	public List<UserJson> getAllActive(
			@RequestHeader(name = "username", required = true) final String username){
		log.trace("Start username={}", username);
		final List<User> users = getUserUseCase.getAllActive(username);
		final List<UserJson> usersJson = mapperUserToJson(users);
		log.trace("End usersJson={}", usersJson);
		return usersJson;
	}

	private List<UserJson> mapperUserToJson(final List<User> users) {
		final List<UserJson> usersJson = new ArrayList<>();
		for (User user : users) {
			final UserJson userJson = UserJson.builder()
					.name(user.getName())
					.build();
			usersJson.add(userJson);
		}
		return usersJson;
	}

	private User mapperUserJsonToUser(final UserJson userJson, Long id) {
		return User.builder()
				.id(id)
				.name(userJson.getName())
				.username(userJson.getUsername())
				.password(userJson.getPassword())
				.votes(new ArrayList<>())
				.userType(userJson.getUserType())
				.active(true)
				.build();
	}
	
	

}
