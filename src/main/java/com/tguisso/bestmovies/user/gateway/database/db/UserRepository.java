package com.tguisso.bestmovies.user.gateway.database.db;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tguisso.bestmovies.user.domain.User;
import com.tguisso.bestmovies.user.domain.UserType;

public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByUsername(String username);

	List<User> findByActiveAndUserType(boolean isActive, UserType userType);

}
