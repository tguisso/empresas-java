package com.tguisso.bestmovies.user.gateway.http.controller.json;

import com.tguisso.bestmovies.user.domain.UserType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "password")
public class UserJson {
	
	private Long id;
	private String name;
	private String username;
	private String password;
	private UserType userType;
	private Boolean active;

}
