package com.tguisso.bestmovies.user.gateway.database.db;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tguisso.bestmovies.movie.gateway.database.exception.ErrorToAccessDatabaseGatewayException;
import com.tguisso.bestmovies.user.domain.User;
import com.tguisso.bestmovies.user.domain.UserType;
import com.tguisso.bestmovies.user.gateway.database.UserDBGateway;
import com.tguisso.bestmovies.user.usecase.exception.UserNotFoundBusinessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class JPAUserDBGateway implements UserDBGateway {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public Long save(User user) {
		log.trace("Start user={}", user);
		try {
			final Long generatedId = userRepository.save(user).getId();
			log.trace("End generatedId={}", generatedId);
			return generatedId;

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public void deleteUserById(Long id) {
		log.trace("Start id={}", id);
		try {
			final Optional<User> userOp = userRepository.findById(id);
			if(userOp.isEmpty()) {
				throw new UserNotFoundBusinessException();
			}
			final User userFound = userOp.get();
			
			final User userToDelete = User.builder()
					.id(userFound.getId())
					.name(userFound.getName())
					.username(userFound.getUsername())
					.password(userFound.getPassword())
					.votes(userFound.getVotes())
					.userType(userFound.getUserType())
					.active(false)
					.build();
			userRepository.save(userToDelete);
			log.trace("End");

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
		
	}

	@Override
	public Optional<User> findUserByUsername(String username) {
		log.trace("Start username={}", username);
		try {
			final Optional<User> userOp = userRepository.findByUsername(username);
			log.trace("End userOp={}", userOp);
			return userOp;

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

	@Override
	public List<User> findAllActiveUser() {
		log.trace("Start");
		try {
			final List<User> users = userRepository.findByActiveAndUserType(true, UserType.USER);
			log.trace("End users={}", users);
			return users;

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}

}
