package com.tguisso.bestmovies.user.gateway.database;

import java.util.List;
import java.util.Optional;

import com.tguisso.bestmovies.user.domain.User;

public interface UserDBGateway {

	Long save(User user);
	void deleteUserById(Long id);
	Optional<User> findUserByUsername(String username);
	List<User> findAllActiveUser();

}
